#!/bin/python3

import os
import sys

#
# Complete the gradingStudents function below.
#
def gradingStudents(grades):
    final_grades = []
    for grade in grades:
        next_mult = ((grade // 5) + 1) * 5
        if (next_mult - grade) < 3 and next_mult > 35:
            final_grades.append(next_mult)
        else:
            final_grades.append(grade)
    return final_grades

if __name__ == '__main__':
    f = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    grades = []

    for _ in range(n):
        grades_item = int(input())
        grades.append(grades_item)

    result = gradingStudents(grades)

    f.write('\n'.join(map(str, result)))
    f.write('\n')

    f.close()

