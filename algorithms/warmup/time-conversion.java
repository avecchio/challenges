import java.io.*;
import java.math.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

public class Solution {

    /*
     * Complete the timeConversion function below.
     */
    static String timeConversion(String s) {
        String timePeriod = s.substring(s.length() - 2, s.length());
        String[] time = s.substring(0, s.length()-2).split(":");
        if (timePeriod.equals("PM")) {
            int hours = Integer.valueOf(time[0]);
            if (hours < 12) {
                time[0] = String.valueOf(hours + 12);
            }
        } else {
            int hours = Integer.valueOf(time[0]);
            if (hours >= 12) {
                time[0] = "00";
            }
        }
        return time[0] + ":" + time[1] + ":" + time[2];
    }

    private static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String s = scan.nextLine();

        String result = timeConversion(s);

        bw.write(result);
        bw.newLine();

        bw.close();
    }
}

