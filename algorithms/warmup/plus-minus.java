import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the plusMinus function below.
    static void plusMinus(int[] arr) {
        double plus = 0.0, minus = 0.0, zero = 0.0;
        double total = (double) arr.length;
        for(int i=0; i<total; i++) {
            if (arr[i] > 0) {
                plus += 1.0;
            } else if (arr[i] < 0) {
                minus += 1.0;
            } else {
                zero += 1.0;
            }
        }
        System.out.println(String.valueOf(plus/total));
        System.out.println(String.valueOf(minus/total));
        System.out.println(String.valueOf(zero/total));
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] arr = new int[n];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        plusMinus(arr);

        scanner.close();
    }
}

